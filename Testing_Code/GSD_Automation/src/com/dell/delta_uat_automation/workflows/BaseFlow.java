package com.dell.delta_uat_automation.workflows;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


import com.dell.delta_uat_automation.utility.CommonUtility;

public class BaseFlow {

	

	public static String alertText;

	public boolean isAlertPresent(WebDriver driver) {
		try {
			Alert a = new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
			if (a != null) {
				System.out.println("Alert is present");
				System.out.println(a.getText());
				driver.switchTo().alert().accept();
				alertText = a.getText();
				return true;
			} else {
				throw new Throwable();
			}
		} catch (Throwable e) {
			// System.err.println("Alert isn't present!!");
			return false;
		}

	}

	public boolean verify(WebDriver driver, WebElement element) {
		boolean present = false;
		if (element != null) {
			CommonUtility.highlightElement(driver, element);
			present = true;
			return present;
		} else {
			return present;
		}
	}

	public boolean verify(WebDriver driver, By by) {
		boolean present = false;
		// WebElement element=driver.findElement(by);
		if (by != null) {
			CommonUtility.highlightElement(driver, by);
			present = true;
			return present;
		} else {
			return present;
		}
	}

	
	public void hovering(WebDriver driver, WebElement a) {
		try {
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			((JavascriptExecutor) driver).executeScript(mouseOverScript, a);
			Thread.sleep(3000);

		} catch (Exception e) {
			System.out.println("Hovering Did Not Happen");
		}
	}

	public boolean isElementVisible(String elementName, By elementType, WebDriver driver) {

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		boolean isVisible = false;

		try {

			wait.until(ExpectedConditions.visibilityOfElementLocated(elementType));

			if (CommonUtility.highlightElement(driver, elementType).isDisplayed()) {

				System.out.println(elementName + ": is displayed");
				isVisible = true;
			} else {

				System.out.println(elementName + ": is not displayed");
				isVisible = false;
				Assert.fail();

			}

		}

		catch (Exception e) {

			System.out.println("Failed while checking visibility of the element:" + elementName);

		}
		return isVisible;

	}

	public static WebDriverWait wait;
	public static int alertcount = 1;

	// generic wait
	public void genericWait(WebDriver driver) {
		wait = new WebDriverWait(driver, 240);
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
	}

}
