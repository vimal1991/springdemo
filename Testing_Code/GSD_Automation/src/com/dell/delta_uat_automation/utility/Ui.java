package com.dell.delta_uat_automation.utility;

public enum Ui {

	SLNo	,
	Region	,
	TestCaseID	,
	Responsibilities	,
	SecurityRegions	,
	ServiceTag	,
	SRTitle	,
	PPID	,
	PartNumber	,
	ActivityType	,
	ActivitySubType	,
	DiagnosisTier1	,
	DiagnosisTier2	,
	DiagnosisTier3	,
	DiagnosisTier4	,
	SRType	,
	AgentDescription	,
	Symptoms	,
	Troubleshooting	,
	Conculsion	,
	Environment	,
	SubEnvironement	,
	RepeatReason	,
	PrimaryContactLastName	,
	PrimaryContactFirstName	,
	PrimaryContactPrimaryPhoneNumber	,
	PrimaryContactEmail	,
	AddressLineText1	,
	AddressLineText2	,
	AddressLineText4	,
	City	,
	State	,
	Country	,
	PostalCode	,
	ServiceType	,
	ServiceOptions	,
	BillTo	,
	ServiceCategory	,
	DSP	,
	DPSType	,
	CallType	,
	ServiceSKU	,
	DLP	,
	InstructionReciever	,
	InstructionType	,
	InstructionComments	,
	Firmware,
	DataImpact,	
	AlternateContactFirstName,	
	AlternateContactLastName,
	AlternateContactPrimaryPhoneNumber,	
	AlternateContactSecondaryPhone,
	AlternateContactEmail,
	ADOverrideReason,
	ScheduleDay,
	AMorPMSelection,
	ScheduleTime,
	SearchEmployeesListAppletBy,
	AddressUpdate,
	KYHD,
	SystemClassification,
	OrderNumber,
    OrderCountry,
    CustomerNumber,
    AccountCountry,
    Period,
    DASP,
    TAMGroup,
    SrNumber,
    SymptomsTier1,
    SymptomsTier2,
    SymptomsTier3,
    Status,
    SearchByValueUnderSRScreen,
    Reference,





	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
