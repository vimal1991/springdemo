package com.hashworks.demo;

import com.hashworks.demo.model.Users;
import com.hashworks.demo.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleTests {

    @Autowired
    private UserRepository userRepository;
    @Test
    public void test1(){
        System.out.print("test sucess");
        int testVal=1;
        Assert.assertEquals(1,testVal);
    }

    @Test
    public void testJpa(){

        Users user=this.userRepository.findByUsername("jack");

        Assert.assertEquals("jack",user.getUserName());
    }

    @Test
    public void testsample(){
        Users alex = new Users("alex","alexPass",1);
        this.userRepository.save(alex);

        Users user=this.userRepository.findByUsername(alex.getUserName());

        Assert.assertEquals(alex.getUserName(),user.getUserName());

        if(this.userRepository.exists(alex.getUserName()))
            this.userRepository.delete(alex);


    }
}
