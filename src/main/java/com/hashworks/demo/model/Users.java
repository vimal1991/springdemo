package com.hashworks.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users  {

    @Id
    private String username;
    private String password;
    private int enabled;


    public Users() {
    }

    public Users(String userName, String password, int enabled) {
        this.username = userName;
        this.password = password;
        this.enabled = enabled;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
