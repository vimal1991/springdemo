package com.hashworks.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@RestController

public class SpringDemoApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SpringDemoApplication.class, args);
    }

//    @RequestMapping("/")
//    String home() {
//        return "Hello World!";
//    }

}
