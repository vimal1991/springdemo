package com.hashworks.demo.repository;

import com.hashworks.demo.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users,String> {

    Users findByUsername(String userName);
}
