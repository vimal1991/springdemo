#!/bin/bash
#installing java
if type -p java; then
	echo found java executable in PATH
	_java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
	echo found java executable in JAVA_HOME     
	_java="$JAVA_HOME/bin/java"
else
    	echo "no java"
    	sudo apt-get update
    	sudo apt-get -y install openjdk-8-jdk
    	echo "java is installed successfully"
fi

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'
sudo apt-get -y install mysql-server
echo "Mysql is installed successfully"

echo "creating the database in mysql"
mysql -u root -p'password' <<EOFMYSQL
create database spring_demo;
EOFMYSQL

echo "Neccessary database is created so now the instance is ready to run the application jar"


echo "Downloading the latest jar in order to bring the application up"
rm -r *.jar
wget --auth-no-challenge --http-user=admin --http-password=32c7ef15ae284c05b23bbb77975c6189 https://ci.hashworks.co/job/Spring%20Demo%20-%20Build/lastSuccessfulBuild/artifact/target/springdemo-1.5.6.RELEASE.jar

killall java

echo "starting the application"
nohup java -jar *.jar > /dev/null 2>&1 &
sleep 330s
echo "Application started successfully"
exit
