variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
default = "/home/hashworks/.ssh/id_rsa.pub"
}

variable "private_key_path" {
  description = "Private key"
  default = "/home/hashworks/.ssh/id_rsa"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
  default = "spbootspring_prod"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-south-1"
}

variable "aws_key" {
   default     = "AKIAISWYHIUQMKDKEDKA"
}

variable "aws_secret" {  
   default     = "ie8lDPYEo4KoUVK6dgeZY2n++XPPKVO3lHJYmSEp"
}

variable "elb_name" {
   default     = "spboot-elb-spring-prod"
}

variable "security_group_name" {
   default     = "sec-group-spboot-spring-prod"
}

variable "num_of_instances" {
   default     = 1
}

variable "elastic_ip_id" {
   default     = "eipalloc-35f69c1b"
}

# Ubuntu Precise 16.04 LTS (x64)
variable "aws_amis" {
  default = {
    ap-south-1 = "ami-099fe766"
  }
}
