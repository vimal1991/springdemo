#!/bin/bash
#installing java
if type -p java; then
	echo found java executable in PATH
	_java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
	echo found java executable in JAVA_HOME     
	_java="$JAVA_HOME/bin/java"
else
    	echo "no java"
    	sudo apt-get update
    	sudo apt-get -y install openjdk-8-jdk
    	echo "java is installed successfully"
fi

killall java
echo "All the existing java apps are killed"

sudo kill -9 $(fuser -n tcp 9098)
echo "Port 9098 is freed to use by the application"

mkdir -p ~/apps
cd ~/apps
rm -rf *.jar
wget --auth-no-challenge --http-user=hashworksadmin --http-password=Hashworks@123# http://ci.hashworks.co/job/AGDemo/56/artifact/target/paketo-1.5.4.RELEASE.25.jar
nohup java -jar *.jar &
echo "Application is sucessfully deployed"

